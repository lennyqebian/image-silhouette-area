/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uploadphoto;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

/**
 *
 * @author Jorke11
 */
public class UploadPhoto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        Form obj=new Form();
//        obj.setVisible(true);
        DataImage obj = new DataImage();

        try {

            InputStream input = new FileInputStream("circulonegro100x100.jpg");
            ImageInputStream imageInput = ImageIO.createImageInputStream(input);

            BufferedImage imagenL = ImageIO.read(imageInput);

            int alto = imagenL.getHeight();
            int ancho = imagenL.getWidth();
            System.out.println("alto " + alto + " ancho " + ancho);

            String content[][] = new String[alto][ancho];
            String rand[][] = new String[alto][ancho];

            String color;
            rand = obj.setArrayRand(alto, ancho);
            int total = 0;

            for (int x = 0; x < alto; x++) {
                for (int y = 0; y < ancho; y++) {

                    int srcPixel = imagenL.getRGB(x, y);
                    Color c = new Color(srcPixel);

                    int valR = c.getRed();
                    int valG = c.getGreen();
                    int valB = c.getBlue();

                    if (valR > 110 && valG > 110 && valB > 110) {
                        color = "0";
                    } else {
                        color = "1";
                    }

                    content[x][y] = color;

//                    System.out.print("r: " + valR + " g: " + valG + " b: " + valB + " /" + color + "/ || ");
                    System.out.print(color);
                }
                System.out.println("");

            }

            total = obj.compare(content, rand, alto, ancho);
            System.out.println("total caidos:"+total);
            System.out.println("Total area: " + ((alto * ancho) * ((double)total / 100)));

        } catch (Exception e) {
        }

    }

}
