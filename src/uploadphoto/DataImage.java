/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uploadphoto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jorke11
 */
public class DataImage {

    public int red, green, blue, randx, randy;
    public String color;

    public DataImage() {
    }

    public DataImage(int red, int green, int blue, String color) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.color = color;
    }

    public DataImage(int randx, int randy) {
        this.randx = randx;
        this.randx = randx;

    }

    public int getColor() {
        return red;
    }

    public void getColor(String color) {
        this.color = color;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public String[][] setArrayRand(int alto, int ancho) {
        String res[][] = new String[alto][ancho];
        int x = 0, y = 0;

        for (int i = 0; i < (alto * ancho); i++) {
            x = (int) (Math.random() * (0 - ancho) + ancho);
            y = (int) (Math.random() * (0 - ancho) + ancho);
            res[x][y] = "1";
        }

        return res;
    }

    public void printArrayRand(String[][] data) {

        for (int i = 0; i < data[0].length; i++) {
            for (int j = 0; j < data.length; j++) {
                if (data[i][j] == null) {
                    System.out.print("0");
                } else {
                    System.out.print(data[i][j]);
                }

            }
            System.out.println("");
        }
    }

    public int compare(String[][] image, String[][] rand, int alto, int ancho) {
        int cont = 0;
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                System.out.println("image[" + i + "][" + j + "] " + image[i][j] + " rand:" + rand[i][j]);
                if (image[i][j].equals("1") && (rand[i][j] != null && rand[i][j].equals("1"))) {
                    cont++;
                }

            }
        }
        return cont;

    }

}
